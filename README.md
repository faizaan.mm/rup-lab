# RUP Lab

All programs and supporting documentation are provided *free of cost* and the owner is **not responsible for any kind of misunderstanding** that may arise due to the use of this code.
Queries will not be entertained. Please **refrain from misuse**.
Create a pull request to contribute.